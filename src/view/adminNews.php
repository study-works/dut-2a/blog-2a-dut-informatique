<!DOCTYPE html>
<html lang="fr">
	<head>
		<?php include 'includes/meta.php'; ?>
		<link rel="stylesheet" href="view/style/admin.css" type="text/css" />
		<link rel="stylesheet" href="view/style/adminNews.css" type="text/css" />

		<title>Admin | ProgWeb</title>
	</head>

	<body>
		<?php
			// Vérification des données provenant du modèle
			if (isset($admin))
			{
		?>
		<header>
			<?php include 'includes/headerAdmin.php'; ?>
		</header>

		<main>
			<section>
				<form method="post">
					<?php
						if (isset($dViewError) && count($dViewError)>0)
						{
					?>
					<div class="alert alert-danger" role="alert">
						<?php
							foreach ($dViewError as $value){
								echo $value.'<br/>';
							}
						?>
					</div>
					<?php
						}
					?>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="title">Titre</label>
						</div>
						<input name="title" type="text" id="title" class="form-control" placeholder="Titre de la news" aria-label="Title" value="<?php if (isset($news)) echo $news->getTitle()?>" required>
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="image">Image</label>
						</div>
						<input name="image" type="text" id="image" class="form-control" placeholder="https://www.exemple.fr/images/mon-image.png" aria-label="Image" value="<?php if (isset($news)) echo $news->getImage()?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="news">News</label>
						</div>
						<textarea name="content" class="form-control contentNews" id="news" aria-label="Contenu de la news" required><?php if (isset($news)) echo $news->getContent()?></textarea>
					</div>
					<div class="custom-control custom-switch">
						<input type="checkbox" name="publishedNews" class="custom-control-input" id="publishedNews" <?php if (isset($news) && $news->getPublished()) echo 'checked="checked"' ?>>
						<label class="custom-control-label" for="publishedNews">Publier la news</label>
					</div>
					<?php
						if (!isset($news))
						{
					?>
					<input type="hidden" name="action" value="addValidation">
					<button type="submit" class="btn btn-primary mt-3">Ajouter</button>
					<?php
						}
						else
						{
					?>
					<input type="hidden" name="action" value="modifyValidation">
					<input type="hidden" name="newsID" value="<?= $news->getID()?>">
					<button type="submit" class="btn btn-primary mt-3">Mettre à jour</button>
					<?php
						}
					?>
				</form>
			</section>
		</main>

		<footer class="mt-auto">
			<?php include 'includes/footer.php'; ?>
		</footer>

		<?php
			}
			else {
				require 'errors/error-401.php';
			}
		?>

		<script src="bootstrap/js/jquery.js"></script>
		<script src="bootstrap/js/bootstrap.js"></script>
	</body>
</html>