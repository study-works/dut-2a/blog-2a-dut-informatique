<?php

class News {
	protected $id;
	protected $title;
	protected $image;
	protected $date;
	protected $content;
	protected $published;

	function __construct(int $id, string $title, string $image, string $date, string $content, bool $published) {
		$this->id=$id;
		$this->title=$title;
		$this->image=$image;
		$this->date=$date;
		$this->content=$content;
		$this->published=$published;
	}

	/**
	 * @return int
	 */
	public function getId(): int { return $this->id; }

	/**
	 * @return string
	 */
	public function getTitle(): string { return $this->title; }

	/**
	 * @return string
	 */
	public function getImage(): string { return $this->image; }

	/**
	 * @return string
	 */
	public function getDate(): string { return $this->date; }

	/**
	 * @return string
	 */
	public function getContent(): string { return $this->content; }

	/**
	 * @return boolean
	 */
	public function getPublished(): bool { return $this->published; }
}