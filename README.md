# Blog DUT Informatique 2A

Ce blog a été réalisé lors de ma deuxième année de DUT Informatique.\
Il s'agit d'un blog réalisé en PHP avec l'architecture MVC.\
L'interface a été réalisé à l'aide de Bootstrap.

## Démonstration
Une démo peut être trouvée [ici](https://www.gabrugiere.net/projets-web/blog-DUT-2A).\
L'adiministration s'accède avec le login et le mot de passe "admin".

## Screenshots
[<img src="screenshots/home.png" alt="Video media player" width=720>](https://gitlab.com/gabrugiere/blog-2a-dut-informatique/-/blob/master/screenshots/home.png)
